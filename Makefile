disable-touchscreens: disable-touchscreens.c
	gcc -Wall -o disable-touchscreens disable-touchscreens.c `pkg-config --libs --cflags glib-2.0 gudev-1.0`

clean:
	rm -f disable-touchscreens
