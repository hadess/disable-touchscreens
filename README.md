# Disable touchscreens on Linux

Compile and run `disable-touchscreens` as root to disable all devices
marked as `ID_INPUT_TOUCHSCREEN` on your system.

## References

- [GNOME bug](https://gitlab.gnome.org/GNOME/mutter/issues/243)
