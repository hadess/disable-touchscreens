/*
 * Copyright (c) 2019 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation.
 *
 * Disables all touchscreens on a Linux system.
 *
 */

/* TODO:
 * Free memory
 */

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <glib.h>
#include <gudev/gudev.h>

static void
disable_touchscreen (GUdevDevice *dev)
{
	GUdevDevice *parent;
	g_autofree char *unbind_path = NULL;
	const char *kernel_name;
	int fd;

	parent = g_udev_device_get_parent_with_subsystem (dev, "hid", NULL);
	if (!parent) {
		g_warning ("Could not find hid parent for %s",
			   g_udev_device_get_sysfs_path (dev));
		return;
	}

	unbind_path = g_build_filename (g_udev_device_get_sysfs_path (parent),
					"driver", "unbind", NULL);
	fd = open (unbind_path, O_WRONLY);
	if (fd < 0) {
		g_warning ("Could not open %s: %s",
			   unbind_path, g_strerror (errno));
		return;
	}
	kernel_name = g_udev_device_get_name (parent);
	if (write (fd, kernel_name, strlen(kernel_name)) < 0) {
		g_warning ("Could not write '%s' to %s: %s",
			   kernel_name, unbind_path, g_strerror (errno));
		close (fd);
		return;
	}
	close (fd);

	g_print ("Successfully disabled %s\n", g_udev_device_get_property (dev, "ID_MODEL"));
}

int main (int argc, char **argv)
{
	GUdevClient *client;
	const gchar * subsystems[] = { "hid", "input", NULL };
	GList *devices, *l;

	client = g_udev_client_new (subsystems);
	devices = g_udev_client_query_by_subsystem (client, "input");
	if (!devices) {
		g_print ("No input devices\n");
		return 0;
	}
	for (l = devices; l != NULL; l = l->next) {
		GUdevDevice *dev = l->data;
		if (!g_udev_device_get_property_as_boolean (dev, "ID_INPUT_TOUCHSCREEN"))
			continue;
		disable_touchscreen (dev);
	}
	return 0;
}
